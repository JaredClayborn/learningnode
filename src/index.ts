import express from 'express';
import bodyParser from 'body-parser';
import { CrmRoutes } from './crm/crm.routes';
import mongoose from 'mongoose';
require('mongoose').Promise = global.Promise;

class App {
    public app: express.Application;
    public routePrv: CrmRoutes = new CrmRoutes();
    public dbUrl: string = 'mongodb+srv://testUser:TestPassword!@learning-096tx.gcp.mongodb.net/test?retryWrites=true';


    constructor() {
        this.app = express();
        this.config();
        this.routePrv.routes(this.app);
        this.mongoSetup();
    }

    private config(): void {
        this.app.use(bodyParser.json());
        // this.app.use(bodyParser.urlencoded({extended:false}));
    }

    private mongoSetup(): void {
        mongoose.set('useFindAndModify', false);
        mongoose.set('useNewUrlParser', true);
        mongoose.connect(this.dbUrl);
    }
}

export default new App().app;