import { CrmController } from './crm.controller';
import app from '../index';
import chai from 'chai';
import chaiHttp from 'chai-http';
import 'mocha';

chai.use(chaiHttp);
const expect = chai.expect;

describe('CrmController', () => {
    describe('get', () => {
        it('should return a 200', () => {
            return chai.request(app).get('/')
                .then(res => {
                    expect(res.status).to.equal(200);
                    expect(res.text).to.contain('Get success');
                });
        });
    });
});