import {Application} from 'express';
import { CrmController } from './crm.controller';
export class CrmRoutes {
    public routes(app: Application): void {
        app.route('/')
            .get(CrmController.get)
            .post(CrmController.create);

        app.route('/contact/:contactId')
            .get(CrmController.getDetails)
            .put(CrmController.update)
            .delete(CrmController.delete);

        app.route('/contact')
            .get(CrmController.getContacts);
    }
}