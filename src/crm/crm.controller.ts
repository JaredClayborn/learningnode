import {Request, Response} from 'express';
import * as mongoose from 'mongoose';
import { ContactSchema } from '../models/Contact';

const Contact = mongoose.model('Contact', ContactSchema);
export class CrmController {
    static get(req: Request, res: Response): any {
        res.status(200).json({message: 'Get success'});
    }

    static getContacts(req: Request, res: Response): any {
        try{
            Contact.find({}, (err, contact) => {
                if (err) {
                    res.status(500).send(err);
                }
                res.status(200).json(contact);
            });
        } catch (ex) {
            res.status(500).send(ex);
        }
    }

    static getDetails(req: Request, res: Response): any {
        try{
            Contact.findById(req.params.contactId, (err, contact) => {
                if (err) {
                    res.status(500).send(err);
                }
                res.status(200).json(contact);
            });
        } catch (ex) {
            res.status(500).send(ex);
        }
    }

    static create(req: Request, res: Response): any {
        let newContact = new Contact(req.body);
        try{
            newContact.save((err, contact) => {
                if (err) {
                    res.status(500).send(err);
                }
                res.status(200).json(contact);
            });
        } catch (ex) {
            res.status(500).send(ex);
        }
    }

    static update(req: Request, res: Response): any {
        try{
            Contact.findOneAndUpdate({ _id: req.params.contactId }, req.body, { new: true }, (err, contact) => {
                if (err) {
                    res.status(500).send(err);
                }
                res.status(200).json(contact);
            });
        } catch (ex) {
            res.status(500).send(ex);
        }
    }

    static delete(req: Request, res: Response): any {
        try {
            Contact.findOneAndDelete({ _id: req.params.contactId }, (err, contact) => {
                if (err) {
                    res.status(500).send(err);
                }
                res.status(200).json(contact);
            });
        } catch (ex) {
            res.status(500).send(ex);
        }
    }


}