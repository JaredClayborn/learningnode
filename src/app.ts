import app from './index';
import cors from 'cors';
import express from 'express';

const port = 3000;
const router = express.Router();
const options: cors.CorsOptions = {
    allowedHeaders: ['Origin', 'X-Requested-With', 'Content-Type', 'Accept', 'X-Access-Token'],
    credentials: true,
    methods: 'GET,HEAD,OPTIONS,PUT,PATCH,POST,DELETE',
    preflightContinue: false
};

router.use(cors(options));
router.options('*', cors(options));

app.listen(port, () => {
    console.log(`Express server up and running on port: ${port}`);
});